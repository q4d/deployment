# deployment

The deployment for q4d app

```text
+----- GIT ------+    +------------+     +-----------+    +---------+
| - terraform    | -> | DEPLOY     |  -> | PROVISION | -> | DESTROY |
| - playbook.yml |    | - validate |     +-----------+    +---------+
|                |    | - plan     |
+----------------+    | - deploy   |
                      +------------+
```