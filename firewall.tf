resource "google_compute_firewall" "default-allow-vote-app" {
  name    = "default-allow-vote-app"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["5000-5020"]
  }

  target_tags = ["q4d-server"]

  source_ranges = ["0.0.0.0/0"]
}

