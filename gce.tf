resource "google_compute_instance" "q4d-server" {
  count = 1
  name = "q4d-server-${count.index}"
  machine_type = "f1-micro"
  zone = "europe-west4-a"

  tags = ["q4d-server"]

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }

  network_interface {
    subnetwork = "default"
    
    access_config {
      // Ephemeral IP
    }
  }

  metadata {
    sshKeys = "ansible:${file(var.ssh_public_key)}"
  }
}
