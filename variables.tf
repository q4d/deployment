variable "project" {
  description = "GCloud Project id"
  type = "string"
}

variable "region" {
  description = "Gcloud Region Netherlands"
  type = "string"
  default = "europe-west4"
}

variable "ssh_public_key" {
  description = "ssh public key"
  type        = "string"
  default = "./keys/ansible_key.pub"
}
