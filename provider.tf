provider "google" {
    credentials = "${file("./creds/serviceaccount.json")}"
    project = "${var.project}"
}
